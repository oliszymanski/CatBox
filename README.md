# CatBox Framework
Framework for positioning and animating using CSS flexbox. Position objects without
even thinking about it!


<br>

## 🤔 What is CatBox exactly?
CatBox is a CSS framework which uses flexbox to position elements such as
`<div>` or `<nav>`. It makes positioning elements on the page easier because
you only need to add a selector to the `class` attribute in html.

<br>




## 👀 How to use CatBox?


### Width levels
**Note:** for every CSS class there is a different width reference.

side-margin-n refers to the space for sections, while card-wn to width
of the whole card. The table beneath shows all possible values, for
both classes:

Class  | side-margin | card-wn
------------- | ------------- | ------------- 
First width value  | 5px | 18rem
Next (nth) width value  |  5px + 3px * n | 18rem + 4rem * n
Max n value | 8 | 3

<br><br>

### Border radius levels
Referring to border radius, this class will work on anything you
desire. For border radius levels I have created this table:


Class | border-rad-n
------------- | -------------
First border-radius value | 2px
Next (nth) border-radius value | 2px + 2px * n

<br><br>

### Section types
Primary section is the split section, which aligns all elements at the
center on x-axis of the page. Default settings:


Queries | row-gap | column-gap 
------------- | ------------- | -------------
without media query | 45px | 0 
with media query | 0 | 25px
